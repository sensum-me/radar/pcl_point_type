#pragma once

#ifndef POINT_XYZVMT_H
#define POINT_XYZVMT_H

#define PCL_NO_PRECOMPILE
#include <pcl/point_types.h>

namespace pcl {
    struct EIGEN_ALIGN16 PointXYZVMT {
    PCL_ADD_POINT4D;
    float v;
    float self_v;
    float snr;
    float rcs;
    std::uint32_t sec;
    std::uint32_t nsec;
    std::uint32_t reserved;
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

}

POINT_CLOUD_REGISTER_POINT_STRUCT(pcl::PointXYZVMT,
    (float, x, x)
    (float, y, y)
    (float, z, z)
    (float, v, v)
    (float, self_v, self_v)
    (float, snr, snr)
    (float, rcs, rcs)
    (std::uint32_t, sec, sec)
    (std::uint32_t, nsec, nsec)
    (std::uint32_t, reserved, reserved)
)

#endif // POINT_XYZVMT_H
